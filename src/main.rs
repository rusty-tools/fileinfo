#![allow(non_snake_case)]

use std::env::args;

pub mod FileInfo;

use MimeInfo::MimeDatabase::MimeDatabase;
use MimeInfo::MimeType::MimeType;

fn getFileMimeType( name: &str, size: u64, mimeDb: &MimeDatabase ) -> MimeType {
    if size > 0 {
        return mimeDb.mimeTypeForFile( name.to_owned() );
    }

    return MimeType::zerosize();
}

fn main() {
    let mimeDb = MimeDatabase::new();

    if args().len() > 1 {
        for arg in args().skip( 1 ) {
            let finfo = FileInfo::FileInfo::new( &arg );
            let minfo: MimeType = match finfo.fileType() {
                FileInfo::FileType::DIRECTORY => {
                    mimeDb.mimeTypeForName( "inode/directory".to_owned() )
                },

                FileInfo::FileType::REGULAR   => {
                    getFileMimeType( &arg, finfo.size(), &mimeDb )
                },

                FileInfo::FileType::CHARDEV   => {
                    mimeDb.mimeTypeForName( "inode/chardevice".to_owned() )
                },

                FileInfo::FileType::FIFO      => {
                    mimeDb.mimeTypeForName( "inode/fifo".to_owned() )
                },

                FileInfo::FileType::SOCKET    => {
                    mimeDb.mimeTypeForName( "inode/socket".to_owned() )
                },

                FileInfo::FileType::BLOCKDEV => {
                    mimeDb.mimeTypeForName( "inode/blockdevice".to_owned() )
                },

                FileInfo::FileType::SYMLINK   => {
                    mimeDb.mimeTypeForName( "inode/symlink".to_owned() )
                },

                _                             => {
                    mimeDb.mimeTypeForName( "application/octet-stream".to_owned() )
                },
            };

            let mut path = FileInfo::normalizePath( &arg );
            let cwd = FileInfo::getCWD();

            /* If the current path is CWD */
            if path != cwd && path.starts_with( &cwd ) {
                path = path.replace( &cwd, "" );
                if path.starts_with( "/" ) {
                    path = path.replacen( "/", "", 1 );
                }
            }

            if finfo.isDir() {
                println!(
                    "{}", match finfo.size() {
                        0 => format!( "{path} (empty)" ),
                        1 => format!( "{path} (1 item)" ),
                        n => format!( "{path} ({} items)", n ),
                    }
                );
            }

            else {
                println!( "{path} ({} bytes)", finfo.size() );
            }

            println!( "    {} ({})\n", minfo.description, minfo.name );
        }
    }
}
