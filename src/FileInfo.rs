#![allow(non_snake_case)]
#![allow(dead_code)]

use std::env;
use std::path::Path;
use std::os::unix::fs::FileTypeExt;
use std::os::unix::fs::PermissionsExt;
/* Use this later to get user/group id. */
// use std::os::linux::fs::MetadataExt;

use std::time::UNIX_EPOCH;
use std::fs;

// use std::os::linux::fs as linux;
// use std::os::unix::fs as unix;

#[derive(Clone, PartialEq)]
pub enum FileType {
    DIRECTORY = 0x001,
    REGULAR   = 0x002,
    CHARDEV   = 0x003,
    FIFO      = 0x004,
    SOCKET    = 0x005,
    BLOCKDEV  = 0x006,
    SYMLINK   = 0x007,
    UNKNOWN   = 0x008,
}

pub enum Permission {
    SUID        = 0o4000,
    SGID        = 0o2000,
    Sticky      = 0o1000,
    ReadOwner   = 0o0400,
    WriteOwner  = 0o0200,
    ExecOwner   = 0o0100,
    ReadGroup   = 0o0040,
    WriteGroup  = 0o0020,
    ExecGroup   = 0o0010,
    ReadOther   = 0o0004,
    WriteOther  = 0o0002,
    ExecOther   = 0o0001,
}

#[derive(Clone)]
pub(crate) struct FileInfo {
    mType: FileType,
    mSize: u64,
    mMtime: u64,
    mPerms: u32,
}

impl FileInfo {
    pub fn new( nameStr: &str ) -> FileInfo {
        /* Get the full path */
        let name = if nameStr.starts_with( "/" ) { nameStr.to_owned() } else { normalizePath( nameStr ) };

        let mut finfo = FileInfo {
            mType: FileType::UNKNOWN,
            mSize: 0,
            mMtime: 0,
            mPerms: 0,
        };

        let path = Path::new( &name );

        let fmeta = match path.metadata() {
            Ok( meta ) => meta,
            Err( _e )   => {
                // eprintln!( "Error: {} ({})", e, &name );
                return finfo;
            },
        };

        let ftype = fmeta.file_type();
        let fperms = fmeta.permissions();

        if ftype.is_dir() {
            finfo.mType = FileType::DIRECTORY;
            finfo.mSize = match fs::read_dir( name ) {
                Ok( nodes ) => match nodes.count().try_into() {
                    Ok( count ) => count,
                    _           => 0,
                },
                _           => 0,
            };
            finfo.mMtime = match fmeta.modified() {
                Ok( time ) => match time.duration_since( UNIX_EPOCH ) {
                    Ok( t ) => t.as_secs(),
                    _       => 0,
                },
                _          => 0,
            };
            finfo.mPerms = fperms.mode();
        }

        else if ftype.is_block_device() {
            finfo.mType = FileType::BLOCKDEV;
            finfo.mSize = 0;
            finfo.mMtime = match fmeta.modified() {
                Ok( time ) => match time.duration_since( UNIX_EPOCH ) {
                    Ok( t ) => t.as_secs(),
                    _       => 0,
                },
                _          => 0,
            };
            finfo.mPerms = fperms.mode();
        }

        else if ftype.is_char_device() {
            finfo.mType = FileType::CHARDEV;
            finfo.mSize = 0;
            finfo.mMtime = match fmeta.modified() {
                Ok( time ) => match time.duration_since( UNIX_EPOCH ) {
                    Ok( t ) => t.as_secs(),
                    _       => 0,
                },
                _          => 0,
            };
            finfo.mPerms = fperms.mode();
        }

        else if ftype.is_fifo() {
            finfo.mType = FileType::FIFO;
            finfo.mSize = 0;
            finfo.mMtime = match fmeta.modified() {
                Ok( time ) => match time.duration_since( UNIX_EPOCH ) {
                    Ok( t ) => t.as_secs(),
                    _       => 0,
                },
                _          => 0,
            };
            finfo.mPerms = fperms.mode();
        }

        else if ftype.is_socket() {
            finfo.mType = FileType::SOCKET;
            finfo.mSize = 0;
            finfo.mMtime = match fmeta.modified() {
                Ok( time ) => match time.duration_since( UNIX_EPOCH ) {
                    Ok( t ) => t.as_secs(),
                    _       => 0,
                },
                _          => 0,
            };
            finfo.mPerms = fperms.mode();
        }

        else if ftype.is_symlink() {
            finfo.mType = FileType::SYMLINK;
            finfo.mSize = 0;
            finfo.mMtime = match fmeta.modified() {
                Ok( time ) => match time.duration_since( UNIX_EPOCH ) {
                    Ok( t ) => t.as_secs(),
                    _       => 0,
                },
                _          => 0,
            };
            finfo.mPerms = fperms.mode();
        }

        else if ftype.is_file() {
            finfo.mType = FileType::REGULAR;
            finfo.mSize = fmeta.len();
            finfo.mMtime = match fmeta.modified() {
                Ok( time ) => match time.duration_since( UNIX_EPOCH ) {
                    Ok( t ) => t.as_secs(),
                    _       => 0,
                },
                _          => 0,
            };
            finfo.mPerms = fperms.mode();
        }

        else {
            finfo.mType = FileType::UNKNOWN;
            finfo.mSize = 0;
            finfo.mMtime = match fmeta.modified() {
                Ok( time ) => match time.duration_since( UNIX_EPOCH ) {
                    Ok( t ) => t.as_secs(),
                    _       => 0,
                },
                _          => 0,
            };
            finfo.mPerms = fperms.mode();
        }

        finfo
    }

    pub fn fileType( &self ) -> FileType {
        self.mType.clone()
    }

    pub fn size( &self ) -> u64 {
        self.mSize
    }

    pub fn lastModifiedAt( &self ) -> u64 {
        self.mMtime
    }

    pub fn permissions( &self ) -> u32 {
        self.mPerms
    }

    pub fn isDir( &self ) -> bool {
        self.mType == FileType::DIRECTORY
    }

    pub fn isFile( &self ) -> bool {
        self.mType == FileType::REGULAR
    }

    pub fn isCharDevice( &self ) -> bool {
        self.mType == FileType::CHARDEV
    }

    pub fn isFifo( &self ) -> bool {
        self.mType == FileType::FIFO
    }

    pub fn isSocket( &self ) -> bool {
        self.mType == FileType::SOCKET
    }

    pub fn isBlockDevice( &self ) -> bool {
        self.mType == FileType::BLOCKDEV
    }

    pub fn isSymlink( &self ) -> bool {
        self.mType == FileType::SYMLINK
    }
}

pub fn getCWD() -> String {
    match env::current_dir() {
        Ok( cwd ) => cwd.display().to_string(),
        _         => match env::var( "HOME" ) {
            Ok( hd ) => hd.to_string(),
            _        => "/".to_owned(),
        },
    }
}

pub fn normalizePath( pathStr: &str ) -> String {
    /* We need a mutable String */
    let mut path = pathStr.to_owned();

    /* CWD */
    let cwd = match env::current_dir() {
        Ok( cwd ) => cwd.display().to_string(),
        _         => "".to_owned(),
    };

    /* Home folder */
    let home = match env::var( "HOME" ) {
        Ok( hd ) => hd.to_string(),
        _        => "".to_owned(),
    };

    /* If the path is simply '.' */
    if path == "." {
        /* Try returning CWD, if ain't empty */
         if !cwd.is_empty() {
            return cwd;
        }

        /* CWD is empty, return home if ain't empty */
        else if !home.is_empty() {
            return home;
        }

        /* Weird environment. Return root. */
        else {
            return "/".to_owned();
        }
    }

    /* If the path is simply '..' */
    if path == ".." {
        /* Try returning CWD, if ain't empty */
         if !cwd.is_empty() {
            return normalizePath( &(cwd + "/..") );
        }

        /* CWD is empty, return home if ain't empty */
        else if !home.is_empty() {
            return normalizePath( &(home + "/..") );
        }

        /* Weird environment. Return root. */
        else {
            return "/".to_owned();
        }
    }

    /* Remove multiple successive separators */
    while path.contains( "//" ) {
        path = path.replace( "//", "/" );
    }

    /* Remove the training '/' */
    if path.ends_with( "/" ) {
        path.pop();
    }

    /* Ensure path begins with '/' by appending CWD/home/root */
    if !path.starts_with( "/" ) {
        /* Try returning CWD, if ain't empty */
         if !cwd.is_empty() {
            path = cwd + "/" + &path;
        }

        /* CWD is empty, return home if ain't empty */
        else if !home.is_empty() {
            path = home + "/" + &path;
        }

        /* Weird environment. Return root. */
        else {
            path = "/".to_owned() + &path;
        }
    }

    /* Now everything is normalized: split the path into components */
    let path_components: Vec<String> = path.split( "/" ).map( |s| s.to_string() ).collect();

    /* This is where we will store our normalized path */
    let mut normPath: Vec<String> = Vec::new();

    // Resolve `.` and `..` components.
    for component in path_components.iter() {
        /* If the current part is '.', do nothing */
        if component == "." {
            continue;
        }

        /* '..' means parent dir: Remove the previous component */
        else if component == ".." {
            normPath.pop();
        }

        else {
            normPath.push( component.to_owned() );
        }
    }

    /* Join the remaining components with a single `/`. */
    normPath.join("/")
}

pub fn basename( path: String ) -> String {
    match normalizePath( &path ).split( "/" ).map( |s| s.to_string() ).collect::<Vec<String>>().pop() {
        Some( part ) => part,
        _            => "/".to_owned(),
    }
}
