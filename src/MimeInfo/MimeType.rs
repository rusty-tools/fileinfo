#![allow(non_snake_case)]

use crate::Utils;

/**
 * The MimeType struct.
 */

#[derive(Clone)]
pub struct MimeType {
    /* Name of the mimetype */
    pub name:         String,
    /* Description of this mimetype */
    pub description:  String,
    /* Icon names that can be used for this mimetype */
    pub iconnames:    (String, String, String),
    /* Other equivale nt names for this mimetype */
    pub aliases:      Vec<String>,
    /* Immediate parents of this mimetype */
    pub ancestors:    Vec<String>,
    /* All parents of this mimetype */
    pub allAncestors: Vec<String>,
}

/**
 * Mimetype static and member functions
 */
impl MimeType {
    /* The ::new() function. Static */
    pub fn new() -> MimeType {
        MimeType {
            name:         "application/octet-stream".to_owned(),
            description:  "unknown".to_owned(),
            iconnames:    Utils::getIconsForOctetStream(),
            aliases:      Vec::new(),
            ancestors:    Vec::new(),
            allAncestors: Vec::new(),
        }
    }

    /* MimeType for application/x-zerosize */
    pub fn zerosize() -> MimeType {
        MimeType {
            name:         "application/x-zerosize".to_owned(),
            description:  "empty file".to_owned(),
            iconnames:    Utils::getIconsForZeroSize(),
            aliases:      Vec::new(),
            ancestors:    vec![ "application/octet-stream".to_owned() ],
            allAncestors: vec![ "application/octet-stream".to_owned() ],
        }
    }

    /** Check if @name is in the list of ancestors */
    pub fn inherits( &self, name: String ) -> bool {
        return self.allAncestors.contains( &name );
    }
}
