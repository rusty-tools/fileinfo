#![allow(non_snake_case)]

pub mod MimeDatabase;
pub mod MimeType;
pub mod Utils;
