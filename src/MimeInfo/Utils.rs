/**
 * This module contains some useful utility functions for the MimeParser module.
**/

use std::io::{BufReader, BufRead};
use std::fs::File;
use std::collections::BTreeMap;
use itertools::Itertools;
use roxmltree::Document;

/** Constants relating to icons **/
const OCTETSTREAM_ICON: &str = "application-octet-stream";
const ZEROSIZE_ICON: &str = "application-x-zerosize";
const APP_GENERIC_ICON: &str = "application-x-generic";
const X_GENERIC: &str = "-x-generic";

/** Linux standard paths **/
const MIMEDIRS: &'static [&'static str] = &[
    "/usr/share/mime/",
    "/usr/local/share/mime/",
    "$HOME/.local/share/mime/"
];

/** Helper function get convert mimetype to icon name **/
fn getIconForMimeType( mimename: String ) -> String {
    mimename.replace( "/", "-" )
}

/** Helper function to get generic icon name from mimetype **/
fn getGenericIconForMimeType( mimename: String ) -> String {
    match mimename.split( "/" ).next() {
        Some( part ) => part.to_owned() + X_GENERIC,
        _            => OCTETSTREAM_ICON.to_owned()
    }
}

/** Helper function to get generic icon name from mimetype **/
fn readFileAsLines( path: String, file: String ) -> Vec<String> {
    let f = match File::open( path + &file ) {
        Ok( f )  => f,
        Err( _ ) => {
            /* Fail silently, because we don't care */
            return Vec::new();
        }
    };

    BufReader::new( f ).lines().map( |s|
        match s {
            Ok( s )  => s.to_string(),
            Err( _e ) => "".to_owned(),
        }
    ).collect()
}

/**
 * Icon triplet for application/octet-stream mimetype
 */
pub(crate) fn getIconsForOctetStream() -> (String, String, String) {
    let icon1: String = OCTETSTREAM_ICON.to_owned();
    let icon2: String = "application-x-generic".to_owned();

    (icon1.clone(), icon1, icon2)
}

/**
 * Icon triplet for application/x-zerosize mimetype
 */
pub(crate) fn getIconsForZeroSize() -> (String, String, String) {
    let icon1: String = ZEROSIZE_ICON.to_owned();
    let icon2: String = OCTETSTREAM_ICON.to_owned();
    let icon3: String = APP_GENERIC_ICON.to_owned();

    (icon1, icon2, icon3)
}

/**
 * All the aliases for a given mimetype
 */
pub(crate) fn getMimeTypeAliases() -> BTreeMap<String, Vec<String>> {
    let mut aliases: BTreeMap<String, Vec<String>> = BTreeMap::new();

    for mimepath in MIMEDIRS {
        for line in readFileAsLines( mimepath.to_string(), "aliases".to_owned() ) {
            /* Ignore comments */
            if line.starts_with( "#" ) {
                continue;
            }

            let parts: Vec<String> = line.split_whitespace().map( |s| s.to_string() ).collect();
            if parts.len() != 2 {
                continue;
            }

            aliases.entry( parts[ 1 ].to_owned() )
                .or_default()
                .push( parts[ 0 ].to_owned() );
        }
    }

    for mimepath in MIMEDIRS {
        for name in readFileAsLines( mimepath.to_string(), "types".to_owned() ) {
            /* Ignore comments */
            if name.starts_with( "#" ) {
                continue;
            }

            aliases.entry( name )
                .or_default()
                .push( "".to_owned() );
        }
    }

    aliases
}

/**
 * Map of all icon triplets for mimetypes
 */
pub(crate) fn getMimeTypeIcons() -> BTreeMap<String, (String, String, String)> {
    let mut icons: BTreeMap<String, (String, String, String)> = BTreeMap::new();

    /* Mimetypes that are defined in the generic-icons file */
    for mimepath in MIMEDIRS {
        for line in readFileAsLines( mimepath.to_string(), "generic-icons".to_owned() ) {
            /* Ignore comments */
            if line.starts_with( "#" ) {
                continue;
            }

            let parts: Vec<String> = line.split( ":" ).map( |s| s.to_string() ).collect();
            if parts.len() != 2 {
                continue;
            }

            /* MimeType name */
            let name         = parts[ 0 ].to_owned();

            /* Icon generated from mimetype */
            let icon         = getIconForMimeType( name.clone() );

            /* Generic icon defined for this MimeType */
            let generic      = parts[ 1 ].to_owned();

            /* Generic icon for mime class (ex: image-x-generic for image class) */
            let mime_generic = getGenericIconForMimeType( name.clone() );

            icons.insert( name, ( icon, generic, mime_generic ) );
        }
    }

    /* Mimetype that are not defined in generic-icons file */
    for mimepath in MIMEDIRS {
        for name in readFileAsLines( mimepath.to_string(), "types".to_owned() ) {
            /* Ignore comments */
            if name.starts_with( "#" ) {
                continue;
            }

            /* Ignore mimetypes that are already added */
            if icons.contains_key( &name ) {
                continue;
            }

            /* Icon generated from mimetype */
            let icon         = getIconForMimeType( name.clone() );

            /* Generic icon for mime class (ex: image-x-generic for image class) */
            let mime_generic = getGenericIconForMimeType( name.clone() );

            icons.insert( name, ( icon, mime_generic.clone(), mime_generic ) );
        }
    }

    icons
}

/**
 * List of all mimetypes
 */
pub(crate) fn getMimeNames() -> Vec<String> {
    let mut mimetypes: Vec<String> = Vec::new();

    /* Mimetype that are not defined in generic-icons file */
    for mimepath in MIMEDIRS {
        mimetypes.append( &mut readFileAsLines( mimepath.to_string(), "types".to_owned() ) );
    }

    mimetypes.into_iter().unique().collect()
}

/**
 * Glob -> MimeType mapping
 */
pub(crate) fn getMimeTypeForGlobs() -> BTreeMap<String, (String, u8)> {
    let mut globs: BTreeMap<String, (String, u8)> = BTreeMap::new();

    /* Mimetypes that are defined in the generic-icons file */
    for mimepath in MIMEDIRS {
        for line in readFileAsLines( mimepath.to_string(), "globs2".to_owned() ) {
            /* Ignore comments */
            if line.starts_with( "#" ) {
                continue;
            }

            let mut parts: Vec<String> = line.split( ":" ).map( |s| s.to_string() ).collect();
            if parts.len() < 3 {
                continue;
            }

            /* Remove the case sensitive tag */
            if parts.len() == 4 && parts[ 3 ] == "cs" {
                parts.pop();
            }

            /* Weight */
            let weight       = match parts[ 0 ].parse::<u8>() {
                Ok( n ) => n,
                _       => 50,
            };

            /* MimeType name */
            let name         = parts[ 1 ].to_owned();

            /* Icon generated from mimetype */
            let glob         = parts[ 2 ].to_owned();

            globs.insert( glob, ( name, weight ) );
        }
    }

    globs
}

/**
 * MimeType -> Glob mapping
 */
pub(crate) fn getGlobsForMimeType() -> BTreeMap<String, Vec<(String, u8)>> {
    let mut globmap: BTreeMap<String, Vec<(String, u8)>> = BTreeMap::new();

    /* Mimetypes that are defined in the generic-icons file */
    for mimepath in MIMEDIRS {
        for line in readFileAsLines( mimepath.to_string(), "globs2".to_owned() ) {
            /* Ignore comments */
            if line.starts_with( "#" ) {
                continue;
            }

            let mut parts: Vec<String> = line.split( ":" ).map( |s| s.to_string() ).collect();
            if parts.len() < 3 {
                continue;
            }

            /* Remove the case sensitive tag */
            if parts.len() == 4 && parts[ 3 ] == "cs" {
                parts.pop();
            }

            /* Weight */
            let weight       = match parts[ 0 ].parse::<u8>() {
                Ok( n ) => n,
                _       => 50,
            };

            /* MimeType name */
            let name         = parts[ 1 ].to_owned();

            /* Icon generated from mimetype */
            let glob         = parts[ 2 ].to_owned();

            globmap.entry( name ).or_default().push( ( glob, weight ) );
        }
    }

    /* Mimetype that are not defined in generic-icons file */
    for mimepath in MIMEDIRS {
        for name in readFileAsLines( mimepath.to_string(), "types".to_owned() ) {
            /* Ignore comments */
            if name.starts_with( "#" ) {
                continue;
            }

            /* Ignore mimetypes that are already added */
            if globmap.contains_key( &name ) {
                continue;
            }

            /* Ignore mimetypes that are already added */
            globmap.entry( name ).or_default().push( ( "".to_string(), 50 ) );
        }
    }

    globmap
}

/**
 * Map of ancestors for mimetypes
 */
pub(crate) fn getSubclassesForMimeTypes() -> BTreeMap<String, Vec<String>> {
    let mut map: BTreeMap<String, Vec<String>> = BTreeMap::new();

    /* Mimetypes that are defined in the subclasses file */
    for mimepath in MIMEDIRS {
        for line in readFileAsLines( mimepath.to_string(), "subclasses".to_owned() ) {
            /* Ignore comments */
            if line.starts_with( "#" ) {
                continue;
            }

            let parts: Vec<String> = line.split( " " ).map( |s| s.to_string() ).collect();
            if parts.len() != 2 {
                // eprintln!( "{:?}", parts );
                continue;
            }

            /* MimeType name */
            let name         = parts[ 0 ].to_owned();

            /* Icon generated from mimetype */
            let superclass   = parts[ 1 ].to_owned();

            map.entry( superclass ).or_default().push( name );
        }
    }

    /* Mimetype that are not defined in generic-icons file */
    for mimepath in MIMEDIRS {
        for name in readFileAsLines( mimepath.to_string(), "types".to_owned() ) {
            /* Ignore comments */
            if name.starts_with( "#" ) {
                continue;
            }

            /* Ignore mimetypes that are already added */
            if map.contains_key( &name ) {
                continue;
            }

            /* Ignore mimetypes that are already added */
            map.entry( name ).or_default().push( "".to_string() );
        }
    }

    map
}

/**
 * Extract the comment from the mimetype xml file
 */
pub(crate) fn getCommentForMimeType( mimetype: String ) -> String {
    for mimepath in MIMEDIRS {
        let xml = match std::fs::read_to_string( mimepath.to_string() + &mimetype.to_lowercase() + ".xml" ) {
            Err( _e ) => {
                // eprintln!( "Error: {} {}{}.xml", e, mimepath, mimetype );
                continue;
            },

            Ok( xml )        => xml,
        };

        let mut opt = roxmltree::ParsingOptions::default();
        opt.allow_dtd = true;
        let doc: Document = Document::parse_with_options( &xml, opt ).unwrap();
        let ns = "http://www.freedesktop.org/standards/shared-mime-info";

        for cnode in doc.descendants().filter( |n| n.is_element() ) {
            if cnode.tag_name().name() == "comment" {
                // TODO: Get the system locate and return the comment in that locale.
                if cnode.attributes().len() == 0 {
                    return match cnode.text() {
                        Some( txt ) => txt.to_string(),
                        _           => "".to_string(),
                    };
                }

                else if cnode.attribute( ( ns, "lang" ) ) == Some( "en" ) {
                    return match cnode.text() {
                        Some( txt ) => txt.to_string(),
                        _           => "".to_string(),
                    };
                }

                else if cnode.attribute( ( ns, "lang" ) ) == Some( "en_US" ) {
                    return match cnode.text() {
                        Some( txt ) => txt.to_string(),
                        _           => "".to_string(),
                    };
                }

                else if cnode.attribute( ( ns, "lang" ) ) == Some( "en_GB" ) {
                    return match cnode.text() {
                        Some( txt ) => txt.to_string(),
                        _           => "".to_string(),
                    };
                }
            }
        }
    }

    "".to_string()
}
