#![allow(non_snake_case)]

use std::collections::BTreeMap;
use std::path::Path;
use globset::{Glob, GlobBuilder, GlobSet, GlobSetBuilder};

use crate::Utils;
use crate::MimeType::MimeType;

/* Typedef :P */
type Icons = (String, String, String);

/**
 * Struct for handling the mimetype database.
 */
#[derive(Clone)]
pub struct MimeDatabase {
    /* List of all mime names */
    mMimeNames:    Vec<String>,

    /* Aliases Map */
    mAliasesMap:   BTreeMap<String, Vec<String>>,

    /* Icons map */
    mIconsMap:     BTreeMap<String, Icons>,

    /* Map of glob vs mimetypes */
    mMimeGlobMap:  BTreeMap<String, (String, u8)>,

    /* Map of mimetype vs globs */
    mGlobMimeMap:  BTreeMap<String, Vec<(String, u8)>>,

    /* List of all globs */
    mGlobs:        Vec<String>,

    /* Map of subclasses for mimetype */
    mAncestorMap:  BTreeMap<String, Vec<String>>,

    /* Map of all mimetypes */
    mMimeTypeMap:  BTreeMap<String, MimeType>,

    /* Case-sensitive globset */
    mCsGlobset:    GlobSet,

    /* Case-insensitive globset */
    mCiGlobset:    GlobSet,
}

/**
 * Struct for handling magic matching rules
 */
#[derive(Clone)]
struct MagicRule {
    /* Begin searching from here */
    offset:   u16,
    /* What to search for */
    value:    Vec<u8>,
    /* Value inversion mask */
    mask:     Vec<u8>,
    /* Search till these many bytes from offset */
    range:    u32,
    /* One or more of these should match */
    subrules: Vec<MagicRule>,
}

impl MimeDatabase {
    /**
     * The new() function
     */
    pub fn new() -> MimeDatabase {
        let mut mdb = MimeDatabase {
            mMimeNames :   Utils::getMimeNames(),
            mAliasesMap :  Utils::getMimeTypeAliases(),
            mIconsMap :    Utils::getMimeTypeIcons(),
            mMimeGlobMap : Utils::getMimeTypeForGlobs(),
            mGlobMimeMap : Utils::getGlobsForMimeType(),
            mGlobs :       Vec::new(),
            mAncestorMap : Utils::getSubclassesForMimeTypes(),
            mMimeTypeMap : BTreeMap::new(),
            mCsGlobset :   GlobSet::empty(),
            mCiGlobset :   GlobSet::empty(),
        };

        mdb.mGlobs = mdb.mMimeGlobMap.keys().cloned().collect();

        for name in &mdb.mMimeNames {
            mdb.mMimeTypeMap.insert( name.clone(), MimeType {
                /* Name of the mimetype */
                name:         name.clone(),
                /* Description of this mimetype */
                description:  Utils::getCommentForMimeType( name.clone() ),
                /* Icon names that can be used for this mimetype */
                iconnames:    mdb.mIconsMap[ &*name ].clone(),
                /* Other equivale nt names for this mimetype */
                aliases:      mdb.mAliasesMap[ &*name ].clone(),
                /* Immediate parents of this mimetype */
                ancestors:    mdb.mAncestorMap[ &*name ].clone(),
                /* All parents of this mimetype */
                allAncestors: Vec::new(),
            } );
        }

        let mut builder_cs: GlobSetBuilder = GlobSetBuilder::new();
        let mut builder_ci: GlobSetBuilder = GlobSetBuilder::new();

        for key in mdb.mGlobs.iter() {
            builder_cs.add( Glob::new( &key ).unwrap() );
            builder_ci.add( GlobBuilder::new( &key ).case_insensitive( true ).build().unwrap() );
        }

        mdb.mCsGlobset = builder_cs.build().unwrap();
        mdb.mCiGlobset = builder_ci.build().unwrap();

        mdb
    }

    fn get_matches( &self, path: &Path ) -> Vec<String> {
        let mut matches: Vec<String> = Vec::new();
        for idx in self.mCsGlobset.matches( path.file_name().unwrap() ) {
            matches.push( match self.mGlobs.get( idx ) {
                Some( m ) => m.to_string(),
                _         => "".to_owned(),
            } );
        }

        if matches.len() > 0 {
            return matches;
        }

        for idx in self.mCiGlobset.matches( path.file_name().unwrap() ) {
            matches.push( match self.mGlobs.get( idx ) {
                Some( m ) => m.to_string(),
                _         => "".to_owned(),
            } );
        }

        return matches;
    }

    /**
     * Returns a list of all the known MimeTypes
     */
    pub fn allMimeTypes( &self ) -> Vec<MimeType> {
        self.mMimeTypeMap.values().cloned().collect()
    }

    /**
     * Returns a list of all MimeTypes for a given file name.
     * Try to match the filenames with the globs and does not
     * check for the contents of the file.
     * The order is according to glob weights, decending order.
     */
    pub fn mimeTypesForFileName( &self, filename: String ) -> Vec<MimeType> {
        let globs = self.get_matches( Path::new( &filename ) );
        let mut mimenames: Vec<(String, u8)> = Vec::new();
        let mut minfos: Vec<MimeType> = Vec::new();

        for glob in globs {
            mimenames.push( self.mMimeGlobMap[ &glob ].clone() );
        }

        mimenames.sort_by( |a, b| b.1.cmp( &a.1 ) );

        for (name, _w) in mimenames {
            minfos.push( self.mimeTypeForName( name ) );
        }

        minfos
    }

    /**
     * Returns a list of all MimeTypes for a given file.
     * First, try to match the filenames with the globs.
     * If that fails, (for ex. Cargo.lock), test the contents.
     * This function assumes that the suffix, and the contents
     * match, and is expected to be reasonably accurate.
     * Speed is a compromise between the two functions:
     * (1) mimeTypesForFileName() -> very fast but less accurate.
     * (2) mimeTypeForFileNameAndContents() -> slow but very accurate.
     */
    pub fn mimeTypeForFile( &self, filename: String ) -> MimeType {
        let mimetypes = self.mimeTypesForFileName( filename );
        if mimetypes.len() > 0 {
            return mimetypes[ 0 ].clone();
        }

        MimeType::new()
    }

    /**
     * Returns a list of all MimeTypes for a given file.
     * (1) Obtain the mimetypes by globs.
     * (2) for each glob, check the corresponding magic.
     * The match with highest priority will be returned.
     * If either of the two steps fail, iterate through
     * all the magics and get the best fitting MimeType.
     * This function can be very slow. Use the function
     * mimeTypeForFile() if such a high level of accuracy
     * is not necessary.
     */
    pub fn mimeTypeForFileNameAndContents( &self, filename: String ) -> MimeType {
        self.mimeTypeForFile( filename )
    }

    /**
     * Returns a list of all MimeTypes for a random data.
     */
    pub fn mimeTypesForData( &self, _data: Vec<u8> ) -> MimeType {
        MimeType::new()
    }

    /**
     * Returns a the MimeType for a given mime name.
     */
    pub fn mimeTypeForName( &self, name: String ) -> MimeType {
        self.mMimeTypeMap[ &name ].clone()
    }

    /**
     * Returns a the best matching suffix the give filename carries
     * Ex: archive.tar.gz   -> tar.gz
     *     archive.test.tgz -> tgz
     */
    pub fn suffixForFileName( &self, name: String ) -> String {
        let mimetype = self.mimeTypeForFileNameAndContents( name );
        let mut suffixes: Vec<(String, u8)> = self.mGlobMimeMap[ &mimetype.name ].clone();

        /* Descending sort */
        suffixes.sort_by( |a, b| b.1.cmp( &a.1 ) );

        /* Get the first suffix that starts with '*.', and remove the */
        // for (sfx, _) in suffixes {
        //     if stx.starts_with
        // }

        "".to_owned()
    }
}

/**
 * This struct contains
 */
#[derive(Clone)]
pub struct MagicMatch {
    /* What exactly we need to match
     * This value can be empty.
     */
    pub matchValue: String,

    /* The range we have to search to match */
    pub matchRange: (u32, u32),

    /* SubMatches */
    pub subMatches: Vec<MagicMatch>,
}

// /**
//  * We will only use filename to get the mime info.
//  * If the filename contains a suffix, it will be matched to the corresponding MimeType.
//  * If a glob is associated with multiple MimeType objects then the first one will be returned.
//  * Use get_MimeTypes_for_file_name to get all the associated MimeType objects.
//  */
// pub fn get_MimeType_for_file_name( filename: &Path ) -> MimeType {
//     let minfos: Vec<MimeType> = get_MimeTypes_for_file_name( filename );
//
//     /* Glob based search: check the globweight only */
//     // let weights: Vec<u8> = minfos.iter().map( |minfo| minfo.globweight ).collect();
//     // let max_idx = weights.iter().position( |&w| w == *weights.iter().max().unwrap() );
//
//     // for minfo in &minfos {
//     //     println!( "{}: {}", minfo.name, minfo.globweight );
//     // }
//
//     // return match max_idx {
//     //     Some(idx) => minfos[ idx ].clone(),
//     //     _         => MimeType::new(),
//     // };
//
//     if minfos.len() > 0 {
//         minfos[ 0 ].clone()
//     }
//
//     else{
//         MimeType::new()
//     }
// }
//
// /**
//  * We will only use filename to get the mime info.
//  * If the filename contains a suffix, it will be matched to the corresponding MimeType.
//  * If a glob is associated with multiple MimeType objects then the first one will be returned.
//  * Use get_MimeTypes_for_file_name to get all the associated MimeType objects.
//  */
// pub fn get_MimeTypes_for_file_name( filename: &Path ) -> Vec<MimeType> {
//     let globs = get_matches( filename );
//     let mut minfos: Vec<MimeType> = Vec::new();
//
//     for glob in globs {
//         let (mname, _w) = MIMEFORGLOB[ &glob ].clone();
//         let mtype = MimeDatabase::mimeTypeForName( mname );
//         minfos.push( mtype );
//     }
//
//     minfos
// }
//
// /**
//  * We will use the filename and data to get the mime info.
//  * If the filename contains a suffix, it will be matched to the corresponding MimeType.
//  * We will then check if the magic also matches. If not, we will try to match all the available
//  * magics from all mimetypes until we hit upon the right one.
//  * If a glob is associated with multiple MimeType objects then, we will check against the magics of
//  * the listed MimeType objects, then move on to others.
//  * Note: This is a potentially expensive operation, do not use it casually.
//  */
// pub fn get_MimeType_for_file( _filename: &Path ) -> MimeType {
//     // let (glob, stem) = get_file_stem_and_glob( filename );
//     // let mimes = &MIMEDB[ &glob ];
//     //
//     // if mimes.len() >= 1 {
//     //     mimes[ 0 ].clone()
//     // }
//     //
//     // else {
//         MimeType::new()
//     // }
// }
